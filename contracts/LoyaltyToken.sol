// SPDX-License-Identifier: MIT
pragma solidity >=0.5.5;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

/**
 * @title Namgay Token Contract
 */
contract LoyalToken is ERC20 {
    string public name = "LoyalToken";
    string public description = "A Token For Remembrance";
    string public symbol = "LT";
    uint8 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 10000000;

    constructor() public {
        _mint(msg.sender, INITIAL_SUPPLY);
    }
}
